#!/bin/bash
set -e

# Atualiza e atualiza o sistema
sudo apt-get update
sudo apt-get upgrade -y

# Instala Apache
sudo apt-get install apache2 -y

# Download e instalação do GitLab Runner
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner

# Cria o usuário GitLab Runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Instala e executa o GitLab Runner como serviço
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

# Registra o GitLab Runner
sudo gitlab-runner register --url https://gitlab.com/ --registration-token GR1348941GY4awvGxTw5ZZVEyAxkL --executor shell --description "Novo Servidor Web" --tag-list "AWS, Apache"

# Dá permissão ao usuário GitLab Runner para o diretório Apache
sudo chown gitlab-runner /var/www/html/ -R
