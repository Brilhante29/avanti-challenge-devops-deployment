resource "aws_instance" "web-server" {
  ami             = "ami-0f30a9c3a48f3fa79"
  instance_type   = "t2.micro"
  key_name        = "Avanti"
  security_groups = ["allow_ssh", "allow_http", "allow_egress"]
  user_data       = file("script.sh")

  tags = {
    Name = "web-server"
  }
}
